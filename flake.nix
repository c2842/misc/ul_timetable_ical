{
	description = "UL Timetable to ical";
  
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";

        nixpkgs-mozilla = {
          url = "github:mozilla/nixpkgs-mozilla";
          flake = false;
        };
  };

  outputs = { self, nixpkgs, flake-utils, naersk, nixpkgs-mozilla }: flake-utils.lib.eachDefaultSystem (system:
    let
        pkgs = (import nixpkgs) {
          inherit system;

          overlays = [
            (import nixpkgs-mozilla)
          ];
        };
      package_name = "ul_ical";

        toolchain = (pkgs.rustChannelOf {
          rustToolchain = ./rust-toolchain.toml;
          sha256 = "sha256-s1RPtyvDGJaX/BisLT+ifVfuhDT1nZkZ1NcK8sbwELM=";
          #        ^ After you run `nix build`, replace this with the actual
          #          hash from the error message
        }).rust;

        naersk' = pkgs.callPackage naersk {
          cargo = toolchain;
          rustc = toolchain;
        };
    in rec {

      # `nix build`
      packages."${package_name}" = naersk'.buildPackage {
        pname = "${package_name}";
        root = ./.;
        
        buildInputs = [
          pkgs.openssl
          pkgs.pkg-config
        ];
      };

      defaultPackage = packages."${package_name}";

      # `nix run`
      apps."${package_name}" = flake-utils.lib.mkApp {
        drv = packages."${package_name}";
      };

      defaultApp = apps."${package_name}";

      # `nix develop`
      devShell = pkgs.mkShell {
        nativeBuildInputs = with pkgs; [ toolchain openssl pkg-config ];
      };
      
      nixosModule = { lib, pkgs, config, ... }: 
        with lib; 
        let
          cfg = config.services."${package_name}";
        in { 
          options.services."${package_name}" = {
            enable = mkEnableOption "enable ${package_name}";
            
            port = mkOption rec {
              type = types.str;
              default = "8066";
              example = default;
              description = "The port to use";
            };

            database = mkOption rec {
              type = types.str;
              default = "database.db";
              example = default;
              description = "The path to the database";
            };
            
            prefix = mkOption rec {
              type = types.str;
              default = "silver_";
              example = default;
              description = "The prefix used to name service/folders";
            };

            user = mkOption rec {
              type = types.str;
              default = "${package_name}";
              example = default;
              description = "The user to run the service";
            };

            home = mkOption rec {
              type = types.str;
              default = "/etc/${cfg.prefix}${package_name}";
              example = default;
              description = "The home for the user";
           };

            
          };

          config = mkIf cfg.enable {
            users.groups."${cfg.user}" = { };

            users.users."${cfg.user}" = {
              createHome = true;
              isSystemUser = true;
              home = "${cfg.home}";
              group = "${cfg.user}";
            };

            systemd.services = {
              "${cfg.prefix}${cfg.user}" = {
                description = "UL Timetable";
                wantedBy = [ "multi-user.target" ];
                after = [ "network-online.target" ];
                wants = [ ];
                serviceConfig = {
                  User = "${cfg.user}";
                  Group = "${cfg.user}";
                  Restart = "always";
                  ExecStart = "${self.defaultPackage."${system}"}/bin/${package_name} ${cfg.database} ${cfg.port} ";
                };
              };
            };

            # for updating the data
            systemd.services."${cfg.prefix}${cfg.user}_update" = {
              description = "Get timetable data";

              wantedBy = [ ];
              after = [ "network-online.target" ];
              serviceConfig = {
                Type = "oneshot";
                User = "${cfg.user}";
                Group = "${cfg.user}";
                ExecStart = "${self.defaultPackage."${system}"}/bin/get_data ${cfg.database}";
              };
            };

            systemd.timers."${cfg.prefix}${cfg.user}_update" = {
              description="Run the get timetable data";

              wantedBy = [ "timers.target" ];
              partOf = [ "${cfg.prefix}${cfg.user}_update.service" ];
              timerConfig = {
                OnCalendar = "10:00:00";
                Unit = "${cfg.prefix}${cfg.user}_update.service";
              };
            };


            systemd.services."${cfg.prefix}${cfg.user}_update_weeks" = {
              description = "Get timetable data - weeks";

              wantedBy = [ ];
              after = [ "network-online.target" ];
              serviceConfig = {
                Type = "oneshot";
                User = "${cfg.user}";
                Group = "${cfg.user}";
                ExecStart = "${self.defaultPackage."${system}"}/bin/get_data_weeks ${cfg.database}";
              };
            };

            systemd.timers."${cfg.prefix}${cfg.user}_update_weeks" = {
              description="Run the get timetable data";

              wantedBy = [ "timers.target" ];
              partOf = [ "${cfg.prefix}${cfg.user}_update_weeks.service" ];
              timerConfig = {
                # every hour this updates teh weeks
                OnCalendar = "*:15";
                Unit = "${cfg.prefix}${cfg.user}_update_weeks.service";
              };
            };

            # this will be run manually (for now?
            systemd.services."${cfg.prefix}${cfg.user}_update_modules" = {
              description = "Get timetable data - modules";

              wantedBy = [ ];
              after = [ "network-online.target" ];
              serviceConfig = {
                Type = "oneshot";
                User = "${cfg.user}";
                Group = "${cfg.user}";
                ExecStart = "${self.defaultPackage."${system}"}/bin/get_data_modules ${cfg.database}";
              };
            };

            systemd.timers."${cfg.prefix}${cfg.user}_update_modules" = {
              description="Run the get timetable data - modules";

              wantedBy = [ "timers.target" ];
              partOf = [ "${cfg.prefix}${cfg.user}_update_modules.service" ];
              timerConfig = {
                OnCalendar = "01:00:00";
                Unit = "${cfg.prefix}${cfg.user}_update_modules.service";
              };
            };
         
            
          };
          
        };
      
    });
}
