use std::env;
use ul_ical::methods::base::week::manage as weeks_manage;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let args: Vec<String> = env::args().collect();
    let database = if args.len() > 1 { &args[1] } else { "database.db" };

    weeks_manage(database).await?;

    Ok(())
}
