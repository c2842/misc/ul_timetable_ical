use std::env;
use ul_ical::methods::classes::module::bulk;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let args: Vec<String> = env::args().collect();
    let database = if args.len() > 1 { &args[1] } else { "database.db" };

    bulk(database).await?;

    Ok(())
}
