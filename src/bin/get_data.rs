use std::env;
use ul_ical::methods::{base::module::module_manage, rooms::rooms_manage};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let args: Vec<String> = env::args().collect();
    let database = if args.len() > 1 { &args[1] } else { "database.db" };

    module_manage(database).await?;
    rooms_manage(database).await;

    Ok(())
}
