// this one has to handle databases

use axum::extract::Query;
use axum::{Extension, Json};
use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::methods::base::{self, db_init, get_semester, get_session, get_timetable, process_calendar, Class, GetTimetable, Weeks};
use chrono::{DateTime, Datelike, Utc};
use form_urlencoded;
use hyper::client::HttpConnector;
use hyper::header::{HeaderName, HeaderValue};
use hyper::{Body, Client, Method, Request};
use hyper_tls::HttpsConnector;
use scraper::{Html, Selector};
use sqlx;
use sqlx::{Error, Pool, Sqlite};
//use chrono::Datelike;

pub async fn rooms_manage(database: &str) {
    let pool = db_init(database).await.unwrap();
    let weeks = base::week::get(&pool, Utc::now().year(), get_semester(&Utc::now())).await.unwrap_or_default();

    let mut result = vec![];
    if let Ok(session) = get_session().await {
        let uri = "https://timetable.ul.ie/UA/RoomTimetable.aspx";
        let headers = HashMap::from([(String::from("cookie"), format!("ASP.NET_SessionId={};", session))]);
        let timetable = get_timetable(&GetTimetable { uri, headers }).await;
        if let Ok(page) = timetable {
            if let Some(form) = process_page(&page) {
                // use this information to get each rooms timetablke

                let https = HttpsConnector::new();
                let client = Client::builder().build::<_, Body>(https);

                for room in &form.rooms {
                    let occupied = get_occupied(&form, uri, &session, &client, room).await;
                    let mut free = get_free(&weeks, &occupied, room);
                    result.append(&mut free);
                }
            }
        }
    }

    match database_manage(&pool, &result).await {
        Ok(_) => {}
        Err(e) => {
            println!("{:#?}", e);
        }
    };
}

#[derive(Debug)]
struct RoomResult {
    view_state: String,
    view_state_generator: String,
    event_validation: String,
    rooms: Vec<String>,
}

fn process_page(page: &str) -> Option<RoomResult> {
    /*
    // can find by id
    __VIEWSTATE
    __VIEWSTATEGENERATOR
    __EVENTVALIDATION



    list of rooms
    option array (like modules)

    __EVENTTARGET: ctl00$HeaderContent$RoomDropdown
    ctl00$HeaderContent$RoomDropdown: <room id>

     */

    let document = Html::parse_document(page);

    // get the rooms fiurst as tis pretty simple

    let mut rooms = vec![];
    if let Ok(option_selector) = Selector::parse("option") {
        let mut options = document.select(&option_selector);

        // skip the first option
        options.next().unwrap();

        for option in options {
            if let Some(x) = option.value().attr("value") {
                rooms.push(x.to_string());
            }
        }
    }

    let mut view_state = Default::default();
    if let Ok(selector) = Selector::parse("#__VIEWSTATE") {
        if let Some(entry) = document.select(&selector).next() {
            if let Some(x) = entry.value().attr("value") {
                view_state = x.to_string();
            }
        }
    }

    let mut view_state_generator = Default::default();
    if let Ok(selector) = Selector::parse("#__VIEWSTATEGENERATOR") {
        if let Some(entry) = document.select(&selector).next() {
            if let Some(x) = entry.value().attr("value") {
                view_state_generator = x.to_string();
            }
        }
    }

    let mut event_validation = Default::default();
    if let Ok(selector) = Selector::parse("#__EVENTVALIDATION") {
        if let Some(entry) = document.select(&selector).next() {
            if let Some(x) = entry.value().attr("value") {
                event_validation = x.to_string();
            }
        }
    }

    if view_state.is_empty() || view_state_generator.is_empty() || event_validation.is_empty() {
        None
    } else {
        Some(RoomResult { view_state, view_state_generator, event_validation, rooms })
    }
}

async fn get_occupied(form: &RoomResult, uri: &str, session: &str, client: &Client<HttpsConnector<HttpConnector>>, room: &str) -> Vec<Class> {
    let body = form_urlencoded::Serializer::new(String::new())
        // took these from the webform
        .append_pair("__VIEWSTATE", &form.view_state)
        .append_pair("__VIEWSTATEGENERATOR", &form.view_state_generator)
        .append_pair("__EVENTVALIDATION", &form.event_validation)
        .append_pair("__EVENTTARGET", "ctl00$HeaderContent$RoomDropdown")
        .append_pair("ctl00$HeaderContent$RoomDropdown", room)
        .finish();

    let mut req = Request::builder().method(Method::POST).uri(uri);
    let headers_request = req.headers_mut().unwrap();

    let headers = [(String::from("cookie"), format!("ASP.NET_SessionId={};", session)), (String::from("Content-Type"), String::from("application/x-www-form-urlencoded"))];

    for (key, val) in headers.iter() {
        headers_request.insert(HeaderName::try_from(key).unwrap(), HeaderValue::from_str(val).unwrap());
    }

    let resp = client.request(req.body(Body::from(body)).unwrap()).await.unwrap();

    let body_bytes = hyper::body::to_bytes(resp.into_body()).await.unwrap();

    let room_page = String::from_utf8(body_bytes.to_vec()).unwrap();

    process_calendar(&room_page)
}

#[derive(Debug, Deserialize, Serialize, sqlx::FromRow)]
pub struct RoomFree {
    room: String,
    year: i32,
    semester: i8,
    week: i8,
    day: i8,
    start: i8,
    end: i8,
    duration: i8,
}

fn get_free(weeks: &HashMap<i8, DateTime<Utc>>, occupied: &[Class], room: &str) -> Vec<RoomFree> {
    let mut result = vec![];
    for week in weeks.keys() {
        for day in 0..=4 {
            let occupied_day = occupied.iter().filter(|entry| (entry.day as i8) == day).collect::<Vec<_>>();

            let mut schedule = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            for entry in occupied_day {
                // only need to deal with classes that are within range
                let mut week_current = false;
                for entry_week in &entry.weeks {
                    match entry_week {
                        Weeks::Range(start, range) => {
                            if week < start {
                                continue;
                            }
                            if week > &(start + range) {
                                continue;
                            }
                            week_current = true;
                        }
                        Weeks::Specific(week_specific) => {
                            if week_specific != week {
                                continue;
                            }
                            week_current = true;
                        }
                    }
                }

                if !week_current {
                    continue;
                }

                let start = if let Ok(x) = entry.time_start[..2].parse::<usize>() {
                    x
                } else {
                    continue;
                };

                let end = if let Ok(x) = entry.time_end[..2].parse::<usize>() {
                    x
                } else {
                    continue;
                };

                for item in schedule.iter_mut().take(end).skip(start) {
                    *item = 1;
                }
            }

            let mut started = true;
            let mut start = 0;
            let mut end = 17;
            for (i, flag) in schedule.iter().enumerate() {
                let mut push = false;
                if started {
                    if flag == &1 {
                        started = false;
                        if i == 0 {
                            continue;
                        }

                        end = i;

                        push = true;
                    } else if i == 17 {
                        end = i + 1;
                        push = true;
                    }
                } else if flag == &0 {
                    started = true;
                    start = i;
                }

                if push {
                    let now = Utc::now();

                    // august is new semester
                    let semester = get_semester(&now);
                    result.push(RoomFree {
                        room: room.to_string(),
                        // breaking change I knpw
                        year: now.year(),
                        semester,
                        week: *week,
                        day,
                        start: start as i8,
                        end: end as i8,
                        duration: (end - start) as i8,
                    });
                }
            }
        }
    }

    result
}

async fn database_manage(pool: &Pool<Sqlite>, result: &[RoomFree]) -> Result<(), Error> {
    if !result.is_empty() {
        database_add(pool, result).await;
    }

    Ok(())
}

async fn database_add(pool: &Pool<Sqlite>, data: &[RoomFree]) {
    for item in data {
        sqlx::query_as::<_, RoomFree>(
            r#"
            INSERT OR REPLACE INTO RoomFree (room, year, semester, week, day, start, end, duration)
            VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)
            "#,
        )
        .bind(&item.room)
        .bind(item.year)
        .bind(item.semester)
        .bind(item.week)
        .bind(item.day)
        .bind(item.start)
        .bind(item.end)
        .bind(item.duration)
        .fetch_optional(pool)
        .await
        .ok();
    }
}

pub async fn get_free_rooms(Query(params): Query<HashMap<String, String>>, Extension(pool): Extension<Pool<Sqlite>>) -> Json<Vec<RoomFree>> {
    let wdh = get_w_d_h(&params, &pool).await;

    if let Ok(results) = sqlx::query_as::<_, RoomFree>(
        r#"
        SELECT *
        FROM RoomFree
        WHERE year == ? AND
              semester == ? AND
              week == ? AND
              day == ?
              AND end > ?
        "#,
    )
    // comment to keep things neat
    .bind(wdh.year)
    .bind(wdh.semester)
    .bind(wdh.week)
    .bind(wdh.day)
    .bind(wdh.hour)
    .fetch_all(&pool)
    .await
    {
        Json(results)
    } else {
        Json(vec![])
    }
}

struct Wdh {
    year: i32,
    semester: i8,
    week: i8,
    day: i8,
    hour: i8,
}

async fn get_w_d_h(params: &HashMap<String, String>, pool: &Pool<Sqlite>) -> Wdh {
    let hour_default = 9;
    let hour = if let Some(x) = params.get("hour") {
        if let Ok(tmp) = x.parse::<i8>() {
            tmp
        } else {
            hour_default
        }
    } else {
        hour_default
    };

    let now = Utc::now();

    let day_tmp = now.date_naive().weekday().num_days_from_monday() as i8;
    // if its weekend show monday
    let day_default = if day_tmp > 4 { 0 } else { day_tmp };
    let day = if let Some(x) = params.get("day") {
        if let Ok(tmp) = x.parse::<i8>() {
            tmp
        } else {
            day_default
        }
    } else {
        day_default
    };

    let year_default = now.year();
    let year = if let Some(x) = params.get("year") {
        if let Ok(tmp) = x.parse::<i32>() {
            tmp
        } else {
            year_default
        }
    } else {
        year_default
    };

    let semester_default = get_semester(&now);
    let semester = if let Some(x) = params.get("semester") {
        if let Ok(tmp) = x.parse::<i8>() {
            tmp
        } else {
            semester_default
        }
    } else {
        semester_default
    };

    let week = if let Some(x) = params.get("week") {
        x.parse::<i8>().unwrap_or(1)
    } else {
        // use current date to get week
        let mut tmp = 1;
        for (week, start) in base::week::get(pool, year, semester).await.unwrap_or_default() {
            if now.timestamp() >= start.timestamp() && week > tmp {
                tmp = week;
            }
        }
        tmp
    };

    Wdh { year, semester, week, day, hour }
}
