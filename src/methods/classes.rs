// this one has to handle databases

use axum::extract::Query;
use axum::{Extension, Json};
use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::methods::base::{self, get_semester};
use chrono::{Datelike, Utc};
use scraper::{Html, Selector};
use sqlx;
use sqlx::{Pool, Sqlite};

pub mod module {
    use crate::methods::base::module::get_modules_vec;
    use crate::methods::base::{db_init, get_semester, get_session, get_timetable, process_calendar, Class, GetTimetable, Weeks};
    use crate::methods::classes::{process_page, RoomResult};
    use axum::extract::Query;
    use axum::{Extension, Json};
    use chrono::{Datelike, Utc};
    use hyper::client::HttpConnector;
    use hyper::header::{HeaderName, HeaderValue};
    use hyper::{Body, Client, Method, Request};
    use hyper_tls::HttpsConnector;
    use serde::{Deserialize, Serialize};
    use sqlx::sqlite::SqliteRow;
    use sqlx::{Error, FromRow, Pool, Row, Sqlite};
    use std::collections::HashMap;
    // https://timetable.ul.ie/UA/ModuleTimetable.aspx
    // ctl00$HeaderContent$DropDownList1

    // this is for the endpoint
    pub async fn get(Query(params): Query<HashMap<String, String>>, Extension(pool): Extension<Pool<Sqlite>>) -> Json<Vec<ClassJson>> {
        let module = if let Some(x) = params.get("module") { x.to_uppercase() } else { "CS4115".to_string() };

        // could convert these into params in the future?
        let year = Utc::now().year();
        let semester = get_semester(&Utc::now());

        Json(get_module(&pool, &module, year, semester, false).await)
    }

    // this is to force refresh
    pub async fn bulk(database: &str) -> Result<(), Error> {
        if let Ok(pool) = db_init(database).await {
            // could convert these into params in the future?
            let year = Utc::now().year();
            let semester = get_semester(&Utc::now());

            for module in get_modules_vec(&pool, year, semester).await {
                get_module(&pool, &module.code, year, semester, true).await;
            }
        }

        Ok(())
    }

    // generic function to handle pulling in/returning teh data for a module
    pub async fn get_module(pool: &Pool<Sqlite>, module: &str, year: i32, semester: i8, force: bool) -> Vec<ClassJson> {
        // try to find it in the db first
        let module_details = if let Some(module_details) = db::get_module(pool, module, year, semester).await {
            if !force {
                let classes = db::get_class(pool, module_details.id).await;
                if !classes.is_empty() {
                    return classes;
                }
            }
            module_details
        } else {
            // if we cannot find it then it does not exist
            // (we scrape in the module details in another cron job)
            return vec![];
        };

        // see if we can pull it out of the database
        if let Ok(session) = get_session().await {
            let uri = "https://timetable.ul.ie/UA/ModuleTimetable.aspx";
            let headers = HashMap::from([(String::from("cookie"), format!("ASP.NET_SessionId={};", session))]);
            if let Ok(page) = get_timetable(&GetTimetable { uri, headers }).await {
                if let Some(form) = process_page(&page) {
                    // use this information to get each rooms timetablke

                    let https = HttpsConnector::new();
                    let client = Client::builder().build::<_, Body>(https);

                    let result = get_module_sub(&form, uri, &session, &client, module).await;
                    // dbg!(&result);
                    db::set_classes(pool, module_details.id, &result).await;

                    // put it into teh db for next time
                    return result;
                }
            }
        }
        vec![]
    }

    pub(super) mod db {
        use sqlx::{Pool, Sqlite};

        use crate::methods::base::module::ModuleDB;
        use crate::methods::classes::module::ClassJson;

        pub async fn get_module(pool: &Pool<Sqlite>, module: &str, year: i32, semester: i8) -> Option<ModuleDB> {
            match sqlx::query_as::<_, ModuleDB>(
                r#"
            SELECT *
            FROM Module
            WHERE year == ? AND semester == ? AND code == ?
        "#,
            )
            // comment to keep things neat
            .bind(year)
            .bind(semester)
            .bind(module)
            .fetch_all(pool)
            .await
            {
                Ok(results) => {
                    if !results.is_empty() {
                        return Some(results[0].to_owned());
                    }
                }
                Err(e) => {
                    dbg!(e);
                }
            }

            None
        }

        pub async fn get_class(pool: &Pool<Sqlite>, id_module: i32) -> Vec<ClassJson> {
            match sqlx::query_as::<_, ClassJson>(
                r#"
            SELECT *
            FROM ModuleClass
            WHERE id_module == ?
        "#,
            )
            // comment to keep things neat
            .bind(id_module)
            .fetch_all(pool)
            .await
            {
                Ok(results) => {
                    return results;
                }
                Err(e) => {
                    dbg!(e);
                }
            }

            vec![]
        }

        pub async fn set_classes(pool: &Pool<Sqlite>, id_module: i32, classes: &Vec<ClassJson>) {
            for item in classes {
                let weeks = item.weeks.iter().map(|&id| id.to_string()).collect::<Vec<_>>();
                match sqlx::query_as::<_, ClassJson>(
                    "
                INSERT OR REPLACE INTO ModuleClass (id_module, start, end, day, module, location, lecturer, event, weeks)
                VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9)
                ",
                )
                .bind(id_module)
                .bind(item.start)
                .bind(item.end)
                .bind(item.day)
                .bind(item.module.to_owned())
                .bind(item.location.to_owned())
                .bind(item.lecturer.to_owned())
                .bind(item.event.to_owned())
                .bind(weeks.join(","))
                .fetch_optional(pool)
                .await
                {
                    Ok(_) => {}
                    Err(e) => {
                        dbg!(e);
                    }
                }
            }
        }
    }

    async fn get_module_sub(form: &RoomResult, uri: &str, session: &str, client: &Client<HttpsConnector<HttpConnector>>, module: &str) -> Vec<ClassJson> {
        let body = form_urlencoded::Serializer::new(String::new())
            // took these from the webform
            .append_pair("__VIEWSTATE", &form.view_state)
            .append_pair("__VIEWSTATEGENERATOR", &form.view_state_generator)
            .append_pair("__EVENTVALIDATION", &form.event_validation)
            .append_pair("__EVENTTARGET", "ctl00$HeaderContent$DropDownList1")
            .append_pair("ctl00$HeaderContent$DropDownList1", module)
            .finish();

        let mut req = Request::builder().method(Method::POST).uri(uri);
        let headers_request = req.headers_mut().unwrap();

        let headers = [(String::from("cookie"), format!("ASP.NET_SessionId={};", session)), (String::from("Content-Type"), String::from("application/x-www-form-urlencoded"))];

        for (key, val) in headers.iter() {
            headers_request.insert(HeaderName::try_from(key).unwrap(), HeaderValue::from_str(val).unwrap());
        }

        let resp = client.request(req.body(Body::from(body)).unwrap()).await.unwrap();

        let body_bytes = hyper::body::to_bytes(resp.into_body()).await.unwrap();

        let room_page = String::from_utf8(body_bytes.to_vec()).unwrap();

        let processed = process_calendar(&room_page);

        processed.iter().map(ClassJson::from).collect::<Vec<_>>()
    }

    #[derive(Debug, Deserialize, Serialize, PartialEq)]
    pub struct ClassJson {
        id: i32,
        id_module: i32,

        // "09:00 - 10:00"
        // 120000
        // 12
        pub start: i32,
        pub end: i32,

        // "EC4004 - LEC"
        pub event: String,
        pub module: String,

        // ALCIC DONAL DR
        pub lecturer: String,

        // "FG061"
        pub location: String,

        pub weeks: Vec<i8>,

        // 0 is monday, 4 is friday
        pub day: i32,
    }

    impl FromRow<'_, SqliteRow> for ClassJson {
        fn from_row(row: &SqliteRow) -> sqlx::Result<Self> {
            let mut weeks = vec![];
            let weeks_row: String = row.try_get("weeks")?;
            for week in weeks_row.split(",") {
                if let Ok(x) = week.parse() {
                    weeks.push(x);
                }
            }

            Ok(Self {
                id: row.try_get("id")?,
                id_module: row.try_get("id_module")?,
                start: row.try_get("start")?,
                end: row.try_get("end")?,
                event: row.try_get("event")?,
                module: row.try_get("module")?,
                lecturer: row.try_get("lecturer")?,
                location: row.try_get("location")?,
                weeks,
                day: row.try_get("day")?,
            })
        }
    }

    impl ClassJson {
        pub fn from(from: &Class) -> Self {
            let mut weeks = vec![];
            for week in &from.weeks {
                match week {
                    Weeks::Range(start, end) => {
                        for i in 0..*end {
                            weeks.push(start + i);
                        }
                    }
                    Weeks::Specific(i) => {
                        weeks.push(*i);
                    }
                }
            }
            weeks.sort();

            Self {
                id: 0,
                id_module: 0,
                start: from.time_start.replace("0000", "").parse().unwrap_or(0),
                end: from.time_end.replace("0000", "").parse().unwrap_or(0),
                event: from.event.to_owned(),
                module: from.module.to_owned(),
                lecturer: from.lecturer.to_owned(),
                location: from.location.to_owned(),
                weeks,
                day: from.day as i32,
            }
        }
    }
}

#[derive(Debug)]
struct RoomResult {
    view_state: String,
    view_state_generator: String,
    event_validation: String,
}
fn process_page(page: &str) -> Option<RoomResult> {
    /*
    // can find by id
    __VIEWSTATE
    __VIEWSTATEGENERATOR
    __EVENTVALIDATION



    list of rooms
    option array (like modules)

    __EVENTTARGET: ctl00$HeaderContent$DropDownList1
    ctl00$HeaderContent$DropDownList1: <room id>

     */

    let document = Html::parse_document(page);

    let mut view_state = Default::default();
    if let Ok(selector) = Selector::parse("#__VIEWSTATE") {
        if let Some(entry) = document.select(&selector).next() {
            if let Some(x) = entry.value().attr("value") {
                view_state = x.to_string();
            }
        }
    }

    let mut view_state_generator = Default::default();
    if let Ok(selector) = Selector::parse("#__VIEWSTATEGENERATOR") {
        if let Some(entry) = document.select(&selector).next() {
            if let Some(x) = entry.value().attr("value") {
                view_state_generator = x.to_string();
            }
        }
    }

    let mut event_validation = Default::default();
    if let Ok(selector) = Selector::parse("#__EVENTVALIDATION") {
        if let Some(entry) = document.select(&selector).next() {
            if let Some(x) = entry.value().attr("value") {
                event_validation = x.to_string();
            }
        }
    }

    if view_state.is_empty() || view_state_generator.is_empty() || event_validation.is_empty() {
        None
    } else {
        Some(RoomResult { view_state, view_state_generator, event_validation })
    }
}

#[derive(Debug, Deserialize, Serialize, sqlx::FromRow)]
pub struct RoomFree {
    room: String,
    year: i32,
    semester: i8,
    week: i8,
    day: i8,
    start: i8,
    end: i8,
    duration: i8,
}

pub async fn get_free_rooms(Query(params): Query<HashMap<String, String>>, Extension(pool): Extension<Pool<Sqlite>>) -> Json<Vec<RoomFree>> {
    let wdh = get_w_d_h(&params, &pool).await;

    if let Ok(results) = sqlx::query_as::<_, RoomFree>(
        r#"
        SELECT *
        FROM RoomFree
        WHERE year == ? AND
              semester == ? AND
              week == ? AND
              day == ?
              AND end > ?
        "#,
    )
    // comment to keep things neat
    .bind(wdh.year)
    .bind(wdh.semester)
    .bind(wdh.week)
    .bind(wdh.day)
    .bind(wdh.hour)
    .fetch_all(&pool)
    .await
    {
        Json(results)
    } else {
        Json(vec![])
    }
}

struct Wdh {
    year: i32,
    semester: i8,
    week: i8,
    day: i8,
    hour: i8,
}

async fn get_w_d_h(params: &HashMap<String, String>, pool: &Pool<Sqlite>) -> Wdh {
    let hour_default = 9;
    let hour = if let Some(x) = params.get("hour") {
        if let Ok(tmp) = x.parse::<i8>() {
            tmp
        } else {
            hour_default
        }
    } else {
        hour_default
    };

    let now = Utc::now();

    let day_tmp = now.date_naive().weekday().num_days_from_monday() as i8;
    // if its weekend show monday
    let day_default = if day_tmp > 4 { 0 } else { day_tmp };
    let day = if let Some(x) = params.get("day") {
        if let Ok(tmp) = x.parse::<i8>() {
            tmp
        } else {
            day_default
        }
    } else {
        day_default
    };

    let year_default = now.year();
    let year = if let Some(x) = params.get("year") {
        if let Ok(tmp) = x.parse::<i32>() {
            tmp
        } else {
            year_default
        }
    } else {
        year_default
    };

    let semester_default = get_semester(&now);
    let semester = if let Some(x) = params.get("semester") {
        if let Ok(tmp) = x.parse::<i8>() {
            tmp
        } else {
            semester_default
        }
    } else {
        semester_default
    };

    let week = if let Some(x) = params.get("week") {
        x.parse::<i8>().unwrap_or(1)
    } else {
        // use current date to get week
        let mut tmp = 1;
        for (week, start) in base::week::get(pool, year, semester).await.unwrap_or_default() {
            if now.timestamp() >= start.timestamp() && week > tmp {
                tmp = week;
            }
        }
        tmp
    };

    Wdh { year, semester, week, day, hour }
}
