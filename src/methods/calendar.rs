use super::base::{get_timetable, module, process_calendar, week, Class, GetTimetable, Weeks};

use axum::extract::Query;
use axum::http::{header, StatusCode};
use axum::response::{IntoResponse, Response};
use axum::Extension;
use chrono::{DateTime, Datelike, Duration, Utc};
use std::collections::HashMap;
use std::ops::Add;
// import without risk of name clashin
use crate::methods::base::get_semester;
use sqlx::{Pool, Sqlite};
use std::fmt::Write as _;

enum Notification {
    Audio,
    Display,
}
struct Reminder<'a> {
    toggle: bool,
    time: &'a str,
    notification: Notification,
}
struct CreateCalendar<'a> {
    weeks_processed: HashMap<i8, DateTime<Utc>>,
    processed: Vec<Class>,
    reminder: Reminder<'a>,
    modules: HashMap<String, String>,
    codes: &'a str,
}

fn create_calendar(details: &CreateCalendar) -> Option<String> {
    let mut ical = "BEGIN:VCALENDAR\nPRODID:-//Brendan.ie//calendar\nVERSION:2.0\nMETHOD:PUBLISH\n".to_string();

    // default colours for teams
    let colours_list = ["Blue", "Green", "Orange", "Purple", "Red", "Yellow"];
    let mut colours = HashMap::new();

    for class in &details.processed {
        let start_week = match class.weeks.first() {
            // this entry has no weeks scheduled
            None => {
                continue;
            }
            Some(week) => match *week {
                Weeks::Range(x, _) => x,
                Weeks::Specific(x) => x,
            },
        };

        let end_week = match class.weeks.last() {
            None => {
                continue;
            }
            Some(week) => match *week {
                Weeks::Range(x, y) => x + y,
                Weeks::Specific(x) => x,
            },
        };

        fn get_date_from_week(class: &Class, weeks_processed: &HashMap<i8, DateTime<Utc>>, start_week: &i8) -> String {
            let date_week_start = match weeks_processed.get(start_week) {
                None => {
                    return String::from("20220905");
                }
                Some(x) => x,
            };
            let date_day = date_week_start.add(Duration::days(class.day));

            date_day.format("%Y%m%d").to_string()
        }

        let day_start = get_date_from_week(class, &details.weeks_processed, &start_week);

        ical.push_str("BEGIN:VEVENT\n");
        ical.push_str("CLASS:PUBLIC\n");

        writeln!(ical, "DESCRIPTION:{}", class.lecturer).unwrap();
        writeln!(ical, "DTSTART:{}T{}", &day_start, class.time_start).unwrap();
        writeln!(ical, "DTEND:{}T{}", &day_start, class.time_end).unwrap();
        writeln!(ical, "LOCATION:{}", class.location).unwrap();

        let event = if details.codes == "name" {
            if let Some(name) = &details.modules.get(&class.module) {
                class.event.replace(&class.module, name)
            } else {
                class.event.to_owned()
            }
        } else if details.codes == "both" {
            if let Some(name) = &details.modules.get(&class.module) {
                class.event.replace(&class.module, &format!("{} ({})", &class.module, name))
            } else {
                class.event.to_owned()
            }
        } else {
            class.event.to_owned()
        };

        writeln!(ical, "SUMMARY:{}", event).unwrap();

        if !colours.contains_key(&class.module) && colours.len() < colours_list.len() {
            // colurors are available
            colours.insert(class.module.clone(), colours_list[colours.len()]);
        }

        if let Some(colour) = colours.get(&class.module) {
            // CATEGORIES:Green category
            writeln!(ical, "CATEGORIES:{} category", colour).unwrap();
        }

        if (end_week - start_week) > 0 {
            // need to get teh weeks not covered and exclude those
            let mut weeks_vec = vec![];
            for week in &class.weeks {
                match week {
                    Weeks::Range(start, range) => {
                        for x in *start..(start + range) {
                            weeks_vec.push(x);
                        }
                    }
                    Weeks::Specific(x) => {
                        weeks_vec.push(*x);
                    }
                }
            }
            weeks_vec.sort();

            let mut excluded_vec = vec![];
            // get teh excluded weeks
            for pair in weeks_vec.windows(2) {
                for x in (pair[0] + 1)..pair[1] {
                    // get the specific excluded day
                    excluded_vec.push(get_date_from_week(class, &details.weeks_processed, &x));
                }
            }

            // need to know how may blank spots to leave
            writeln!(ical, "RRULE:FREQ=WEEKLY;COUNT={}", (end_week - start_week) + (excluded_vec.len() as i8)).unwrap();

            if !excluded_vec.is_empty() {
                writeln!(ical, "EXDATE:{}", excluded_vec.join(",")).unwrap();
            }
        }

        if details.reminder.toggle {
            ical.push_str("BEGIN:VALARM\n");
            writeln!(ical, "TRIGGER:-PT{}M", &details.reminder.time).unwrap();
            match &details.reminder.notification {
                Notification::Audio => {
                    ical.push_str("ACTION:AUDIO\n");
                }
                Notification::Display => {
                    ical.push_str("ACTION:DISPLAY\n");
                }
            }
            ical.push_str("ACTION:DISPLAY\n");
            ical.push_str("DESCRIPTION:Reminder\n");
            ical.push_str("END:VALARM\n");
        }

        writeln!(ical, "UID:{}{}{}{}", &day_start, class.time_start, class.time_end, Utc::now().timestamp_nanos_opt().unwrap_or(0)).unwrap();
        ical.push_str("END:VEVENT\n");

        // DTSTAMP and SEQUENCE do not seem to replace existing items with same UID

        // BEGIN:VEVENT
        // CLASS:PUBLIC
        // DESCRIPTION:{lecturer}
        // DTSTART:{day start}T{time start}
        // DTEND:{day start}T{time end}
        // LOCATION:{location}
        // SUMMARY:{event}
        // RRULE:FREQ=WEEKLY;COUNT=12;BYDAY=MO
        // UID:040000008200E00074C5B7101A82E00800000000C021476D65B6D80100000000000
        // END:VEVENT
    }

    // handle the week reminders ehre
    for (week, date) in details.weeks_processed.iter() {
        let date_start_raw = date.add(Duration::days(0));
        let date_start = date_start_raw.format("%Y%m%d").to_string();

        let date_end_raw = date.add(Duration::days(5));
        let date_end = date_end_raw.format("%Y%m%d").to_string();
        let now = Utc::now().timestamp_nanos_opt().unwrap_or(0);
        writeln!(
            ical,
            r#"BEGIN:VEVENT
CLASS:PUBLIC
TRANSP:TRANSPARENT
DESCRIPTION:
DTSTART;VALUE=DATE:{date_start}
DTEND;VALUE=DATE:{date_end}
LOCATION:University of Limerick
SUMMARY:Week {week}
UID:{week}{date_start}{date_end}{now}
END:VEVENT"#
        )
        .unwrap();
    }

    ical.push_str("END:VCALENDAR\n");

    Some(ical)
}

pub async fn get_ical(Query(params): Query<HashMap<String, String>>, Extension(pool): Extension<Pool<Sqlite>>) -> Response {
    let session_id = if let Some(x) = params.get("ical_session_id") { x } else { return (StatusCode::BAD_REQUEST, "No session_id").into_response() };
    let forms_auth = if let Some(x) = params.get("ical_forms_auth") { x } else { return (StatusCode::BAD_REQUEST, "No forms_auth").into_response() };
    let ical_type = if let Some(x) = params.get("ical_type") { x } else { "student" };
    let codes = if let Some(x) = params.get("ical_codes") { x } else { "code" };
    let reminder_toggle = if let Some(x) = params.get("ical_reminder_toggle") { x == "yes" } else { false };
    let reminder_time = if let Some(x) = params.get("ical_reminder_time") { x } else { "10" };
    let mut reminder_type = Notification::Display;
    if let Some(x) = params.get("ical_reminder_type") {
        if x == "audio" {
            reminder_type = Notification::Audio;
        }
    };

    let uri = if ical_type == "student" { "https://timetable.ul.ie/StudentTimetable.aspx" } else { "https://timetable.ul.ie/LecturerTimetable.aspx" };

    let calendar_raw = get_timetable(&GetTimetable { uri, headers: HashMap::from([(String::from("cookie"), format!("ASP.NET_SessionId={}; .ASPXFORMSAUTH={}", session_id, forms_auth))]) }).await.unwrap();

    let processed = process_calendar(&calendar_raw);
    if processed.is_empty() {
        return (StatusCode::NO_CONTENT, "No Classes, maybe invalid details?").into_response();
    }

    if let Some(calendar) = create_calendar(&CreateCalendar {
        // commented to format
        weeks_processed: week::get(&pool, Utc::now().year(), get_semester(&Utc::now())).await.unwrap_or_default(),
        processed,
        reminder: Reminder { toggle: reminder_toggle, time: reminder_time, notification: reminder_type },
        modules: module::get(&pool, Utc::now().year(), get_semester(&Utc::now())).await.unwrap_or_default(),
        codes,
    }) {
        (StatusCode::OK, [(header::CONTENT_TYPE, "text/calendar; charset=utf-8"), (header::CONTENT_DISPOSITION, "attachment; filename=brendan.ie_ul_calendar.ics")], calendar).into_response()
    } else {
        (StatusCode::NO_CONTENT, "Week data not available yet, try again in a few days time.").into_response()
    }
}
