use chrono::{DateTime, Datelike, Month, Utc};
use hyper::header::{HeaderName, HeaderValue};
use hyper::{Body, Client, Method, Request};
use hyper_tls::HttpsConnector;
use regex::Regex;
use scraper::{Html, Selector};
use serde::{Deserialize, Serialize};
use sqlx::sqlite::{SqliteConnectOptions, SqlitePoolOptions};
use sqlx::{Error, Pool, Sqlite};
use std::collections::HashMap;
use std::str::FromStr;

#[derive(Clone)]
pub struct GetTimetable<'a> {
    pub uri: &'a str,
    pub headers: HashMap<String, String>,
}

pub async fn get_general(details: &GetTimetable<'_>) -> Result<String, Box<dyn std::error::Error + Send + Sync>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);

    let resp = client.get(details.uri.parse()?).await?;

    let body_bytes = hyper::body::to_bytes(resp.into_body()).await?;
    Ok(String::from_utf8(body_bytes.to_vec())?)
}

pub async fn get_session() -> Result<String, Box<dyn std::error::Error + Send + Sync>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);

    let uri = "https://timetable.ul.ie/UA/ModuleTimetable.aspx";

    // // first requesty
    let resp_initial = client.get(uri.parse()?).await?;

    // follow redirect
    let location = resp_initial.headers().get("location").unwrap().to_str().unwrap();
    let resp_redirect = client.get(format!("https://timetable.ul.ie{}", location).parse()?).await?;

    // ASP.NET_SessionId=dcedowzze1wirfoltcjjhv5f; path=/; HttpOnly
    let cookie = String::from(resp_redirect.headers().get("set-cookie").unwrap().to_owned().to_str()?).replace("ASP.NET_SessionId=", "").replace(" SameSite=Lax", "").replace("; path=/; HttpOnly", "");
    Ok(cookie)
}

pub async fn get_timetable(details: &GetTimetable<'_>) -> Result<String, Box<dyn std::error::Error + Send + Sync>> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, Body>(https);

    let mut req = Request::builder().method(Method::GET).uri(details.uri);
    let headers = req.headers_mut().unwrap();

    for (key, val) in details.headers.iter() {
        headers.insert(HeaderName::try_from(key)?, HeaderValue::from_str(val)?);
    }

    let resp = client.request(req.body(Body::from("Hallo!")).unwrap()).await?;

    let body_bytes = hyper::body::to_bytes(resp.into_body()).await?;
    Ok(String::from_utf8(body_bytes.to_vec())?)
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub enum Weeks {
    Range(i8, i8),
    Specific(i8),
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct Class {
    // "09:00 - 10:00"
    // 120000
    pub time_start: String,
    pub time_end: String,

    // "EC4004 - LEC"
    pub event: String,
    pub module: String,

    // ALCIC DONAL DR
    pub lecturer: String,

    // "FG061"
    pub location: String,

    pub weeks: Vec<Weeks>,

    // 0 is monday, 4 is friday
    pub day: i64,
}

pub fn process_calendar(page: &str) -> Vec<Class> {
    let document = Html::parse_document(page);
    let tr_selector = Selector::parse("tr").unwrap();
    let td_selector = Selector::parse("td").unwrap();

    let mut table = document.select(&tr_selector);

    // skip the first row
    // if there is no first row just return an empty vec
    if table.next().is_none() {
        return vec![];
    }

    // thanks to yuki and Z
    // <br /> -> <br> , seems like teh scraper does its own stuff
    //                                        (?P<start_time>\d{2}:\d{2}) - (?P<end_time>\d{2}:\d{2})<br>(?P<course>[^<]*)<br>(?P<professor>[^<]*)<br>(?P<room>[^<]*)?(<br>)?Wks:(?P<weeks>[^<]*)(<br>)?((<span class="online">)?(?P<online>ONLINE))?
    let entry_regex = Regex::new(r"(?P<start_time>\d{2}:\d{2}) - (?P<end_time>\d{2}:\d{2})<br>(?P<course>[^<]*)<br>(?P<professor>[^<]*)<br>(?P<room>[^<]*)?(<br>)?Wks:(?P<weeks>[^<]*)(<br>)?(.*(?P<online>ONLINE))?").unwrap();

    //let mut result = HashMap::new();
    let mut result = vec![];
    for row in table {
        for (day, col) in row.select(&td_selector).enumerate() {
            let inner_raw = col.inner_html();
            let inner = inner_raw.trim();
            if !inner.is_empty() {
                for entry in inner.split("<br> <br>").collect::<Vec<_>>() {
                    let caps = if let Some(caps) = entry_regex.captures(entry) {
                        caps
                    } else {
                        continue;
                    };

                    let time_start = &caps.name("start_time").map_or("", |m| m.as_str()).replace(':', "");
                    let time_end = &caps.name("end_time").map_or("", |m| m.as_str()).replace(':', "");

                    let event = caps.name("course").map_or("", |m| m.as_str()).trim();
                    let event_split = event.split(" - ").collect::<Vec<_>>();
                    let module = event_split[0].trim();

                    let lecturer = caps.name("professor").map_or("", |m| m.as_str()).trim();
                    let location_raw = caps.name("room").map_or("", |m| m.as_str()).trim();
                    let week_raw = caps.name("weeks").map_or("", |m| m.as_str()).trim();

                    let location = if !location_raw.is_empty() { location_raw } else { caps.name("online").map_or("", |m| m.as_str()).trim() };

                    let mut weeks_vec = vec![];
                    for week_data in week_raw.replace("Wks:", "").split(',').collect::<Vec<_>>() {
                        if week_data.find('-').is_some() {
                            let weeks = week_data.split('-').collect::<Vec<_>>();
                            let start = weeks[0].parse::<i8>().unwrap_or(1);
                            let end = weeks[1].parse::<i8>().unwrap_or(12);
                            weeks_vec.push(Weeks::Range(start, end - start + 1));
                        } else {
                            weeks_vec.push(Weeks::Specific(week_data.parse::<i8>().unwrap_or(1)));
                        };
                    }

                    result.push(Class {
                        time_start: format!("{}00", time_start),
                        time_end: format!("{}00", time_end),
                        event: event.to_owned(),
                        module: module.to_string(),
                        lecturer: lecturer.to_owned(),
                        location: location.to_owned(),
                        weeks: weeks_vec,
                        day: day as i64,
                    });
                }
            }
        }
    }
    result
}

pub mod module {
    use crate::methods::base::{db_init, get_semester, get_session, get_timetable, GetTimetable};
    use chrono::{Datelike, Utc};
    use scraper::{Html, Selector};
    use serde::{Deserialize, Serialize};
    use sqlx::{Error, Pool, Sqlite};
    use std::collections::HashMap;

    #[derive(Debug, Deserialize, Serialize, sqlx::FromRow, Clone)]
    pub struct ModuleDB {
        pub id: i32,
        year: i32,
        semester: i8,
        pub code: String,
        name: String,
    }

    pub async fn module_manage(database: &str) -> Result<(), Error> {
        if let Ok(pool) = db_init(database).await {
            let data = process().await;
            add(&pool, &data).await
        }

        Ok(())
    }

    async fn process() -> Vec<ModuleDB> {
        let session = get_session().await.unwrap();
        let page = get_timetable(&GetTimetable { uri: "https://timetable.ul.ie/UA/ModuleTimetable.aspx", headers: HashMap::from([(String::from("cookie"), format!("ASP.NET_SessionId={};", session))]) }).await.unwrap();

        let document = Html::parse_document(&page);
        let option_selector = Selector::parse("option").unwrap();
        let mut options = document.select(&option_selector);

        // skip the first option
        options.next().unwrap();

        let mut result = vec![];
        for option in options {
            let data_raw = option.inner_html();
            let data_split = data_raw.split(" - ").collect::<Vec<_>>();

            result.push(ModuleDB {
                // formatting
                id: 0,
                year: Utc::now().year(),
                semester: get_semester(&Utc::now()),
                code: data_split[0].to_string(),
                name: data_split[1].to_string(),
            });
        }

        result
    }

    async fn add(pool: &Pool<Sqlite>, data: &[ModuleDB]) {
        for item in data {
            sqlx::query_as::<_, ModuleDB>(
                "
                INSERT OR REPLACE INTO Module (year, semester, code, name)
                VALUES (?1, ?2, ?3, ?4)
                ",
            )
            .bind(item.year)
            .bind(item.semester)
            .bind(&item.code)
            .bind(&item.name)
            .fetch_optional(pool)
            .await
            .ok();
        }
    }

    pub async fn get(pool: &Pool<Sqlite>, year: i32, semester: i8) -> Result<HashMap<String, String>, Error> {
        let mut tmp = HashMap::new();

        for result in get_modules_vec(pool, year, semester).await {
            tmp.insert(result.code, result.name);
        }

        Ok(tmp)
    }

    pub async fn get_modules_vec(pool: &Pool<Sqlite>, year: i32, semester: i8) -> Vec<ModuleDB> {
        match sqlx::query_as::<_, ModuleDB>(
            r#"
            SELECT *
            FROM Module
            WHERE year == ? AND semester == ?
        "#,
        )
        // comment to keep things neat
        .bind(year)
        .bind(semester)
        .fetch_all(pool)
        .await
        {
            Ok(result) => result,
            Err(e) => {
                dbg!(e);
                vec![]
            }
        }
    }
}

pub mod week {
    use crate::methods::base::{db_init, get_general, get_semester, mon_to_month, GetTimetable};
    use axum::{Extension, Json};
    use chrono::{DateTime, MappedLocalTime, TimeZone, Utc};
    use scraper::{Html, Selector};
    use serde::{Deserialize, Serialize};
    use sqlx::{Error, Pool, Sqlite};
    use std::collections::HashMap;

    #[derive(Debug, Deserialize, Serialize, sqlx::FromRow)]
    struct WeekDB {
        year: i32,
        semester: i8,
        week: i8,
        date: DateTime<Utc>,
    }

    pub async fn manage(database: &str) -> Result<(), Error> {
        if let Ok(pool) = db_init(database).await {
            let weeks = process().await;
            add(&pool, &weeks).await
        }

        Ok(())
    }

    async fn process() -> Vec<WeekDB> {
        let page = get_general(&GetTimetable { uri: "https://timetable.ul.ie/UA/WeekDate.aspx", headers: Default::default() }).await.unwrap();

        let document = Html::parse_document(&page);
        let tr_selector = Selector::parse("tr").unwrap();
        let td_selector = Selector::parse("td").unwrap();
        let font_selector = Selector::parse("font").unwrap();

        let mut table = document.select(&tr_selector);

        // skip the first row
        table.next().unwrap();

        let mut result = vec![];
        for row in table {
            let mut col = row.select(&td_selector);

            let date_raw = col.next().unwrap().select(&font_selector).next().unwrap().inner_html();
            let date_split = date_raw.split(' ').collect::<Vec<_>>();
            // no need for middle
            col.next().unwrap();

            let week = col.next().unwrap().select(&font_selector).next().unwrap().inner_html().parse::<i8>().unwrap_or(0);

            let day = date_split[0].parse::<u32>().unwrap_or(1);
            let month = mon_to_month(date_split[1]);
            let year = date_split[2].parse::<i32>().unwrap_or(1);
            let date = match Utc.with_ymd_and_hms(year, month.number_from_month(), day, 0, 0, 0) {
                MappedLocalTime::Single(a) => a,
                MappedLocalTime::Ambiguous(a, _) => a,
                MappedLocalTime::None => Utc::now(),
            };

            let semester = get_semester(&date);

            result.push(WeekDB { year, semester, week, date });
        }

        result
    }

    async fn add(pool: &Pool<Sqlite>, data: &[WeekDB]) {
        for item in data {
            sqlx::query_as::<_, WeekDB>(
                "
                INSERT OR REPLACE INTO Week (year, semester, week, date)
                VALUES (?1, ?2, ?3, ?4)
                ",
            )
            .bind(item.year)
            .bind(item.semester)
            .bind(item.week)
            .bind(item.date)
            .fetch_optional(pool)
            .await
            .ok();
        }
    }

    pub async fn get(pool: &Pool<Sqlite>, year: i32, semester: i8) -> Result<HashMap<i8, DateTime<Utc>>, Error> {
        let mut tmp = HashMap::new();

        if let Ok(results) = sqlx::query_as::<_, WeekDB>(
            r#"
            SELECT *
            FROM Week
            WHERE year == ? AND semester == ?
        "#,
        )
        // comment to keep things neat
        .bind(year)
        .bind(semester)
        .fetch_all(pool)
        .await
        {
            for result in results {
                tmp.insert(result.week, result.date);
            }
        }

        Ok(tmp)
    }

    #[derive(Debug, Deserialize, Serialize)]
    pub struct WeekResponse {
        year: i32,
        semester: i8,
        weeks: Vec<i8>,
    }
    pub async fn get_timeframe(Extension(pool): Extension<Pool<Sqlite>>) -> Json<Vec<WeekResponse>> {
        let mut tmp = vec![];

        if let Ok(results) = sqlx::query_as::<_, WeekDB>("SELECT * FROM Week").fetch_all(&pool).await {
            let mut temp: HashMap<i32, HashMap<i8, Vec<i8>>> = HashMap::new();

            for result in results {
                temp.entry(result.year).or_default();

                if let Some(inner) = temp.get_mut(&result.year) {
                    inner.entry(result.semester).or_insert_with(Vec::new);

                    if let Some(semester) = inner.get_mut(&result.semester) {
                        semester.push(result.week);
                    }
                }
            }

            // Iterate over everything.
            for (year, inner) in &temp {
                for (semester, weeks) in inner {
                    tmp.push(WeekResponse { year: *year, semester: *semester, weeks: weeks.to_owned() });
                }
            }
        }

        Json(tmp)
    }
}

// 3 letter month to Month enum
fn mon_to_month(mon: &str) -> Month {
    match mon {
        "Jan" => Month::January,
        "Feb" => Month::February,
        "Mar" => Month::March,
        "Apr" => Month::April,
        "May" => Month::May,
        "Jun" => Month::June,
        "Jul" => Month::July,
        "Aug" => Month::August,
        "Sep" => Month::September,
        "Oct" => Month::October,
        "Nov" => Month::November,
        "Dec" => Month::December,
        &_ => Month::January,
    }
}

pub async fn db_init(database: &str) -> Result<Pool<Sqlite>, Error> {
    let pool = SqlitePoolOptions::new()
        // formatting
        .max_connections(5)
        .connect_with(SqliteConnectOptions::from_str(&format!("sqlite://{}", database))?.foreign_keys(true).create_if_missing(true))
        .await?;

    // migrations are amazing!
    sqlx::migrate!("./db/migrations").run(&pool).await?;

    Ok(pool)
}

pub(crate) fn get_semester(date: &DateTime<Utc>) -> i8 {
    if date.month() > 7 {
        1
    } else {
        2
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // <td>
    // 09:00 - 11:00<br />CS4457 - LEC<br /> ABHISHEK OOMMEN JACOB<br /> HSG037<br />Wks:1-8,10-13
    // </td>
    // <td>
    // 09:00 - 10:00<br />CS4106 - LEC<br /> MALACHY EATON<br /> KBG10<br />Wks:1-8,10-13
    // </td>

    const INPUT: &str = r#"
    <table>
        <tr></tr>
        <tr>
          <td>
            09:00 - 10:00<br />CS4457 - LAB - 2D<br /> ABHISHEK OOMMEN JACOB<br /> CS1044<br />Wks:1-8,10-13
          </td>
          <td> </td>
          <td> </td
          ><td> </td>
        </tr>
    </table>
    "#;

    const RESULT_STRING: [[&str; 6]; 1] = [["090000", "100000", "CS4457 - LAB - 2D", "CS4457", "ABHISHEK OOMMEN JACOB", "CS1044"]];
    const RESULT_WEEKS: [[Weeks; 2]; 1] = [[Weeks::Range(1, 8), Weeks::Range(10, 4)]];

    #[test]
    fn test_process() {
        for (index, result) in process_calendar(INPUT).iter().enumerate() {
            assert_eq!(RESULT_STRING[index][0], &result.time_start);
            assert_eq!(RESULT_STRING[index][1], &result.time_end);
            assert_eq!(RESULT_STRING[index][2], &result.event);
            assert_eq!(RESULT_STRING[index][3], &result.module);
            assert_eq!(RESULT_STRING[index][4], &result.lecturer);
            assert_eq!(RESULT_STRING[index][5], &result.location);

            for (index_weeks, result) in result.weeks.iter().enumerate() {
                assert_eq!(RESULT_WEEKS[index][index_weeks], *result);
            }
        }
    }

    const INPUT2: &str = r#"
    <table>
        <tr></tr>
        <tr>
          <td>
            11:00 - 13:00<br>BM4002 - LEC<br><br> B1023<br>Wks:19,14-17<br> <br>11:00 - 13:00<br>BM4002 - LEC<br><br> FG042<br>Wks:1-11,13
          </td>
          <td> </td>
          <td> </td
          ><td> </td>
        </tr>
    </table>
    "#;

    const RESULT_STRING2: [[&str; 6]; 2] = [["110000", "130000", "BM4002 - LEC", "BM4002", "", "B1023"], ["110000", "130000", "BM4002 - LEC", "BM4002", "", "FG042"]];
    const RESULT_WEEKS2: [[Weeks; 2]; 2] = [[Weeks::Specific(19), Weeks::Range(14, 4)], [Weeks::Range(1, 11), Weeks::Specific(13)]];

    #[test]
    fn test_process2() {
        for (index, result) in process_calendar(INPUT2).iter().enumerate() {
            assert_eq!(RESULT_STRING2[index][0], &result.time_start);
            assert_eq!(RESULT_STRING2[index][1], &result.time_end);
            assert_eq!(RESULT_STRING2[index][2], &result.event);
            assert_eq!(RESULT_STRING2[index][3], &result.module);
            assert_eq!(RESULT_STRING2[index][4], &result.lecturer);
            assert_eq!(RESULT_STRING2[index][5], &result.location);

            for (index_weeks, result) in result.weeks.iter().enumerate() {
                assert_eq!(RESULT_WEEKS2[index][index_weeks], *result);
            }
        }
    }
}
