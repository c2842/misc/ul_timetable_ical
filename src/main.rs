use axum::routing::get;
use axum::{Extension, Router};
use std::env;
use ul_ical::methods::classes::module;
use ul_ical::methods::{base::db_init, base::week::get_timeframe, calendar::get_ical, rooms::get_free_rooms};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let args: Vec<String> = env::args().collect();
    let database = if args.len() > 1 { &args[1] } else { "database.db" };
    let port = if args.len() > 2 { args[2].parse::<u16>().unwrap_or(8066) } else { 8066 };

    let pool = db_init(database).await?;

    let listener = tokio::net::TcpListener::bind(format!("127.0.0.1:{port}")).await.unwrap();
    let app = Router::new()
        // comment to keep it compact
        .route("/timetable/", get(|| async { "Hello, World!" }))
        .route("/timetable/calendar", get(get_ical))
        .route("/timetable/rooms", get(get_free_rooms))
        .route("/timetable/timeframe", get(get_timeframe))
        .route("/timetable/classes/module", get(module::get))
        .layer(Extension(pool));

    println!("Starting server on port {port}");
    axum::serve(listener, app).await.unwrap();

    Ok(())
}
