
-- need to add an ID we can reference
-- so create new table
CREATE TABLE ModuleTmp (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    year        INTEGER NOT NULL,
    semester    INTEGER NOT NULL,
    code        TEXT    NOT NULL,
    name        TEXT    NOT NULL,
    UNIQUE (year, semester, code)
);
-- migrate data to it
INSERT INTO ModuleTmp (year, semester, code, name)
SELECT year, semester, code, name
FROM Module;

-- delete old table and rename new to old
DROP TABLE Module;
ALTER TABLE ModuleTmp RENAME TO Module;


-- a place to store the details of every class
CREATE TABLE ModuleClass (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    id_module   INTEGER NOT NULL,
    start       INTEGER NOT NULL,
    end         INTEGER NOT NULL,
    day         INTEGER NOT NULL,
    module      TEXT    NOT NULL,
    event       TEXT    NOT NULL,
    location    TEXT    NOT NULL,
    lecturer    TEXT    NOT NULL,
    weeks       TEXT    NOT NULL,

    FOREIGN KEY(id_module) REFERENCES Module(id)
);


