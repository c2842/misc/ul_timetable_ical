-- Didnt have this initially


CREATE TABLE IF NOT EXISTS RoomFree (
    room text NOT null,
    year integer NOT null,
    semester integer NOT null,
    week integer NOT null,
    day integer NOT null,
    start integer NOT null,
    end integer NOT null,
    duration integer NOT null,
    UNIQUE (room, year, semester, week, day, start)
);
CREATE INDEX IF NOT EXISTS week_day ON RoomFree (year, semester, week, day);


CREATE TABLE IF NOT EXISTS Module (
    year integer NOT null,
    semester integer NOT null,
    code text NOT null,
    name text NOT null,
    UNIQUE (year, semester, code)
);

CREATE TABLE IF NOT EXISTS Week (
    year integer NOT null,
    semester integer NOT null,
    week integer NOT null,
    date datetime NOT null,
    UNIQUE (year, semester, week)
);

